<?php
	include("header.php");
	include("connect.php");
	include("geocoder.php");
	
	//Google Maps usefulness: http://jsfiddle.net/salman/4mtyu/
	//Server side geocoding: http://erlycoder.com/45/php-server-side-geocoding-with-google-maps-api-v3
?>

<?php
	//Geocode
	if (!empty($_POST["location"])) { 
		$address=urlencode($_POST["location"]);
		$loc = geocoder::getLocation($address);
		$zoomLevel = 10;
	}	

	else {
		//$address=urlencode("1810 N 39 Ave. Hollywood, FL");
		$loc = array (
			'lat' => 38.0000,
			'long' => -97.0000
		);
		$zoomLevel = 5;
	};
	
	
	if (!empty($_POST["location"])) { 
		$address=urlencode($_POST["location"]);
		$loc = geocoder::getLocation($address);
	}	

	else {
		$address=urlencode("1810 N 39 Ave. Hollywood, FL");
		$loc = geocoder::getLocation($address);
	};

	/*
	//Debugging
	echo "<h2>";
	print_r($loc);
	echo "</h2>";
	*/

?>
	
<h2>Search Results</h2>
<hr size="1" />
<h3><a href=".."><img src="media/arrow-back-icon.png" /></a> <a href="..">Search again</a></h3>

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&amp;sensor=false"></script>
<!-- BEGIN GEOCODER -->


<script type="text/javascript">
/*
var geocoder = new google.maps.Geocoder();
var address = "<?php echo $_POST["location"]; ?>";

geocoder.geocode( { 'address': address}, function(results, status) {

if (status == google.maps.GeocoderStatus.OK) {
	var latitude = results[0].geometry.location.lat();
	var longitude = results[0].geometry.location.lng();
    }
}); 
*/
/* GOOGLE MAP SCRIPT */
var map;
google.maps.event.addDomListener(window, "load", function () {

//var latitude = 46.1000;

/* SET THE ZOOM AND CENTER HERE */
var map = new google.maps.Map(document.getElementById("map_div"), {

    center: new google.maps.LatLng(<?php echo $loc["lat"]; ?>, <?php echo $loc["lng"]; ?>),
    zoom: <?php echo $zoomLevel ?>,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

var infoWindow = new google.maps.InfoWindow();
function createMarker(options, html) {
    var marker = new google.maps.Marker(options);
    if (html) {
      google.maps.event.addListener(marker, "click", function () {
        infoWindow.setContent(html);
        infoWindow.open(options.map, this);
      });
    }
    return marker;
  }

/* BEGIN ADDING MARKERS TO MAP USING DATABASE QUERY */   
<?php

$i = 0; //counter for marker numbers.



//UPDATE THIS DATABSE QUERY TO USE USER INPUT.
if ($result = $link->query("SELECT * FROM listing ORDER BY price ASC")) {
    //printf("Showing %d rows.\n", $result->num_rows);

	while($row = $result->fetch_assoc()){
   
   //var marker0 = createMarker({
   //position: new google.maps.LatLng(26.0519444, -80.1441667), map: map, icon: "http://1.bp.blogspot.com/_GZzKwf6g1o8/S6xwK6CSghI/AAAAAAAAA98/_iA3r4Ehclk/s1600/marker-green.png"  }, "<p class='map-title'>Elementary Statistics</p><p class='map-price'>$13</p>");
   
   
	echo 'var marker' . $i . ' = createMarker({
    position: new google.maps.LatLng('. $row['lat'] . ', ' . $row['lng'] . '), map: map, icon: "http://1.bp.blogspot.com/_GZzKwf6g1o8/S6xwK6CSghI/AAAAAAAAA98/_iA3r4Ehclk/s1600/marker-green.png"  }, "<p class=\'map-title\'><b>' . $row['title'] . '</b></p><p class=\'map-price\'>$' . $row['price'] . '</p><a class=\'map-link\' href=\'listing.php?id=' . $row['listing_id'] . '\'>View listing</a>");';
	
	$i++; //Increment marker counter
	}
    $result->close();
}
?>
  
});

</script>
<div width="100%" style="text-align: center;">
	<div id="map_div" style="width: 100%; height: 400px;" ></div>
</div>

<style type="text/css">
.product-list-thumbnail:hover {
border: 10px solid #CFCFCB !important;
}
</style>

<?php
if ($result = $link->query("SELECT * FROM listing JOIN books ON listing.isbn=books.ISBN ORDER BY price ASC")) {
    //printf("Showing %d rows.\n", $result->num_rows);

	echo "<br><center>
	<table><tr style='border-bottom: 1px solid #999999'><td colspan='2' align='center'><h3>Books Near You</h3></td></tr>";
	
	while($row = $result->fetch_assoc()){
   
	echo "<tr style='border-bottom: 1px solid #999999'><td><a  href='listing.php?id=" . $row['listing_id'] . "'><img class='product-list-thumbnail' src='book-thumbnails/" . $row['thumbnailURL'] ."' width='60' style='margin: 35px 12px; border: 10px solid #ECECEA;' /></a></td><td><b><span style='font-size: 18px; margin-bottom: 15px;'>" . $row['title'] . "</span><br>" . "<span style='color: #33CC33; font-size: 20px;'>$" . $row['price'] . "</span></b><br>" .  $row['city'] . ", " . $row['state'] . "<br><br><span style='background-color: #0088CC; padding: 8px;'><a style='color:#fff;' href='listing.php?id=" . $row['listing_id'] . "'>View listing>></a></span></td></tr>";
	
	$i++; //Increment marker counter
	}
    $result->close();
	
	echo "</table></center>";
}
	mysqli_close($link);
?>

<?php
	include("footer.php");
?>

