<?php 
	include('header.php'); 

?>

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="hero-unit">
        <h1><img src="media/book-icon.png" /> Textbook City</h1>
        <h4 style="margin: 25px 0; font-size: 19px;"><br>Find textbooks in your area. Sell to locals. Keep more cash. All for free.</h4>
		
		<form action="results.php" method="post">

		Search by Book Title or ISBN Number: <input type="text" name="title" placeholder="Quantum Mechanics"><br>
		Location: <input type="text" name="location" placeholder="Fort Lauderdale, FL"><br>
		<input class="btn btn-primary btn-large" type="submit" value="Search">
		</form>
        <!-- <p><a href="#" class="btn btn-primary btn-large">Learn more &raquo;</a></p> -->
      </div>

      <!-- Example row of columns -->
      <div class="row">
        <div class="span4">
          <h2>Sell Your Book</h2>
          <p>Post your book for free. Meet up with another student. Get cash. No evil bookstore fees. No shipping drama.</p>
          <p><a class="btn" href="listing_form.php">Post your book now&raquo;</a></p>
        </div>
        <div class="span4">
          <h2>Buy a Book</h2>
          <p>See who's selling that book you need in the area, way cheaper than bookstores. All your cash goes to fellow students.</p>
          <p><a class="btn" href="buybook.php">Find a book &raquo;</a></p>
       </div>
        <div class="span4">
          <h2>Want to help students?</h2>
          <p>Become a consigner in your area, where students can drop off and pick up books at an exchange hosted by your group.</p>
          <p><a class="btn" href="consignment.php">View details &raquo;</a></p>
        </div>
      </div>

<?php include('footer.php') ?>
