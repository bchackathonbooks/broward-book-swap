<?php
	include("header.php");
	include("connect.php");
	include("geocoder.php");

?>
	

<h2>Book Details</h2>
<hr size="1" />
<h3><a href=".."><img src="media/arrow-back-icon.png" /></a> <a href="..">Search again</a></h3>

<?php 
	if (isset($_POST['messagesubmitted'])) {
		echo '<h4 style="color: #33CC33;" class="notification-message">Your message has been sent.</h4>';
	};
?>


<?php


if ($result = $link->query("SELECT * FROM listing WHERE listing_id=" . $_GET['id'])) {
    //printf("Showing %d rows.\n", $result->num_rows);
	
	
	
	while($row = $result->fetch_assoc()){
   
		
		if ($result2 = $link->query("SELECT * FROM books WHERE ISBN=" . $row['isbn'])) {
			while($row2 = $result2->fetch_assoc()){
				$thumbnail = $row2['thumbnailURL'];
				$author = $row2['author'];
			};
		};
		$description = $row['description'];
		$price = $row['price'];
		$title = $row['title']; 
		$city = $row['city']; 
		$isbn = $row['isbn'];
		$state = $row['state'];
	
	};
    $result->close();
	
}
	mysqli_close($link);
	
?>


<div style="font-size: 14px;">
	<h3><?php echo $title; ?></h3>
	<img src="book-thumbnails/<?php echo $thumbnail; ?>" style="float: left; padding: 0 12px 12px 0;">
	<p><b>This Seller's Price</b></p>
	<h3><span style="background-color: #0088CC; color: #FFF; padding: 8px;">$<?php echo $price; ?></span></h3>
	
	<p><b>Author(s):</b> <?php echo $author; ?></p>
	<p><b>ISBN:</b> <?php echo $isbn; ?></p>
	<p><b>Location:</b> <?php echo $city . ", " . $state ?></p>
	<p><b>Description:</b> <?php echo $description; ?></p>
	
	<hr size="1" />
	
	<h3>Contact the Seller</h3>
	
	<form action="listing.php?id=<?php echo $_GET['id'];?>" method="post">
		<input type="hidden" name="messagesubmitted" value="true" />

		Subject Line:<br> <input type="text" name="subjectline"><br>
		Message:<br>
		<textarea name="message" rows="10" cols="30" placeholder="Enter your message to the seller"></textarea><br>
		<input class="btn btn-primary btn-large" type="submit" value="Send Message" />
	</form>
	
	
	
</div>

<?php
	include("footer.php");
?>

