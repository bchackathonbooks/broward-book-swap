<?php 
	include('header.php'); 

?>



	  
	  <div class="consignerlibrary">
	  <h1>Consigner Sign up </h1>
	  </div>

	  
	  
	  
</br></br>
 <div class="para">
<div class="medium-6 large-8 columns">
<h3>About</h3>
<hr size="1" />
<div style="font-size: 16px; font-weight: normal;">
<p>College textbooks are extremely expensive and put a big dent in the pockets of college in need of every cent they can lay their hands on. Becoming a consigner is a great way to make college more affordable for students.</p>

<p> You will provide students with a physical space to sell their textbooks. Students will be able to drop off the books at your location so that the organization can manage the sale of the book, making sure it is sold for a fair price and the transaction goes as smooth as possible.</p>

<p>For those concerned with meeting a stranger this also acts as a location where they can meet up.</p>

<p>Textbook City can facilitate the sale of the books so that the organization can be as efficient as possible and it will also help the organization earn more popularity exposing it to people searching for or selling books within the area. 
</p>
</div>
</div>
</div>
                
            </div>
        </div>
    </div>
	</br></br></br>
	<section id="contact">
        <div class="container">
            <div class="jumbotron">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Contact us</h2>
                    <hr class="primary">
                    <p>Are you interested in our consigner program? Give us a call or send us an email and we will get back to you as soon as possible!</p>
					<a class = "btn btn-default">Sign Up!</a>
			   </div>
                <div class="col-lg-4 col-lg-offset-2 text-center">
                    <i class="fa fa-phone fa-3x wow bounceIn"></i>
                    <p>954-417-9340</p>
                </div>
                <div class="col-lg-4 text-center">
                    <i class="fa fa-envelope-o fa-3x wow bounceIn" data-wow-delay=".1s"></i>
                    <p><a href="mailto:your-email@your-domain.com">tbookswappers@gmail.com</a></p>
                </div>
            </div>
        </div>
    </section>


<?php include('footer.php') ?>
